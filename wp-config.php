<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'booker-bs' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']9Udji@/oOhwir^l92)P-b7s9X,=yf.VedY7sF`)v]_cc<(x;n]<).pHhEa@*ybv' );
define( 'SECURE_AUTH_KEY',  'Lm*9i{PB8 8Q#4iG[#%h[4.BDBy^,Dd-cV-_]xVVW&:3]v}AM>O6d^l~lNQo$h:K' );
define( 'LOGGED_IN_KEY',    'Cqkp=|bh=^pr9sUh)@!pB)cWG1yR;dDJw^xPCT+8-Wa`Ch@igtu<RvYqGZu,BF43' );
define( 'NONCE_KEY',        'Q+/vI;<@X.a5_^rfUrcF,HahI9nb##!N0rB?Z6e.+ XF,Ale+][=:e@purEpE7-d' );
define( 'AUTH_SALT',        'urM)XP?B@y%vW;[W.LH21f68..xO[~Lfw7<X4tp76wAl{&E2+Um(9b6bGgqO-oEM' );
define( 'SECURE_AUTH_SALT', 'lh%[]jc5Ne0H--wowx&fUvX:canrdKnMZ!TfA6e2A|_W]N}NVCO)nx!%pVmOh[Qn' );
define( 'LOGGED_IN_SALT',   '0CBx$zvMGs8Mi/H2EmH|JmVK#]D+!-vhCTB`hkip>e7i:vBm^yF.bB>uk&yFVGvr' );
define( 'NONCE_SALT',       '4Q#]=n|%T?Sv.I%4lVd0j/b3GbCqZ+E(lH7ff1(w6TN 9o4*I*Gl<lunm;irk*J(' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
